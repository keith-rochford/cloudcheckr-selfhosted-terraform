// Use this file to populate variables for the CloudCheckr Self Hosted deployment

//image_id = "ami-abc123"
//availability_zone_names = [
  //"us-east-1a",
 // "us-west-1c",
//]

// CC-VPC
cc_sh_vpc_id = "vpc-e7233b83"

// CC-Web-Console
cc_web_ami = "ami-0bbc25e23a7640b9b"
cc_web_type = "t2.micro"

// CC-DB
cc_sh_db_paramgrp_family = "sqlserver-se-12.0"
cc_sh_db_allocated_storage = "500"
cc_sh_db_engine = "sqlserver-se"
cc_sh_db_engine_version = "SQL Server 11.00.6020.0.v1"
cc_sh_rds_instance_class = "db.m4.large"
cc_sh_db_name = "CloudCheckr-DB"
cc_sh_db_username = "ChengeMe"
cc_sh_db_password = "ChangeMe"
cc_sh_rds_multi_az = true
cc_sh_allowed_vpc_cidr_blocks

