// Create the RDS MS SQL instance for CloudCheckr self hosted

// Create the RDS DB Subnets
// TODO: Parameterise the CIDR blocks
resource "aws_subnet" "cc_sh_db_subnt1" {
  vpc_id     = "${var.cc_sh_vpc_id}"
  cidr_block = "172.31.48.0/23"
}
resource "aws_subnet" "cc_sh_db_subnt2" {
  vpc_id     = "${var.cc_sh_vpc_id}"
  cidr_block = "172.31.50.0/23"
}

// RDS DB Subnet Group
resource "aws_db_subnet_group" "cc_sh_db_subnet_grp" {
   //name = "cc-rds-${var.environment}-dbsubnet"
   name = "cc_rds_dbsubnet_grp"
   description = "Subnet Group for Self-hosted CloudCheckr ${var.environment}} DB"
   subnet_ids = ["${aws_subnet.cc_sh_db_subnt1.id}", "${aws_subnet.cc_sh_db_subnt2.id}"]
}

// RDS DB parameter group (Must enabled triggers to allow Multi-AZ)
resource "aws_db_parameter_group" "cc_sh_db_allow_triggers" {
   //name = "cc-rds-${var.environment}-allow-triggers"
   name = "cc-rds-allow-triggers"
   family = "${var.cc_sh_db_paramgrp_family}" //"sqlserver-se-12.0"
   description = "Parameter Group for Self-hosted CloudCheckr ${var.environment} to allow triggers"

   parameter {
     name = "log_bin_trust_function_creators"
     value = "1"
   }
}

// RDS
resource "aws_db_instance" "cc_sh_rds_db" {
  allocated_storage    = "${var.cc_sh_db_size}"
  engine               = "${var.cc_sh_db_engine}"
  engine_version       = "${var.cc_sh_db_engine_version}"
  instance_class       = "${var.cc_sh_db_instance}"
  //identifier           = "CloudCheckr-db"
  name                 = "${var.cc_sh_db_name}"
  username             = "${var.cc_sh_db_username}"
  password             = "${var.cc_sh_db_password}"
  db_subnet_group_name = "${aws_db_subnet_group.cc_sh_db_subnet_grp.id}"
  parameter_group_name = "${aws_db_parameter_group.cc_sh_db_allow_triggers.id}"
  multi_az           = "${var.db_multiaz}"
  vpc_security_group_ids = ["${aws_security_group.cc_sh_db_sg.id}"]
  #availability_zone     = "${var.vpc_az1}"
  publicly_accessible    = "false"
  backup_retention_period = "2"

  apply_immediately = "true"
}



// RDS Security Group
// TODO: Parameterise the CIDR blocks
resource "aws_security_group" "cc_sh_db_sg" {
  name        = "cc_sh_db_sg"
  description = "Allow traffic to CloudCheckr RDS DB"
  vpc_id      = "${var.cc_sh_vpc_id}"

  ingress {
    # TLS (change to whatever ports you need)
    from_port   = 443
    to_port     = 443
    protocol    = "-1"
    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["192.168.10.22/32"] # add a CIDR block here
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    //prefix_list_ids = ["pl-12c4e678"]
  }
}