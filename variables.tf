// Use this file to declare variables for the CloudCheckr Self Hosted deployment

// Environment name, used as prefix to name resources.
variable "environment" {
    default = "Sovereign-Dev"
}

//GLOBAL
variable "cc_sh_region" {
  default = "eu-west-1"
}

// CC-Web-Console
variable "cc_web_ami" {
  //default = "ami-0bbc25e23a7640b9b"
}
variable "cc_web_type" {
  default = "m4.xlarge"
}

// CC-Scheduler
variable "cc_scheduler_ami" {
  //default = "ami-0bbc25e23a7640b9b"
}
variable "cc_scheduler_type" {
  default = "m4.xlarge"
}

// CC-DB
variable "cc_sh_db_paramgrp_family" {
  default = "sqlserver-se-12.0"
}

variable "cc_sh_db_allocated_storage" {
  default = ""
}
variable "cc_sh_db_engine" {
  default = ""
}
variable "cc_sh_db_engine_version" {
  default = ""
}
variable "cc_sh_rds_instance_class" {
  default = ""
}
variable "cc_sh_db_name" {
  default = ""
}
variable "cc_sh_mssql_admin_username" {
  default = ""
}
variable "cc_sh_mssql_admin_password" {
  default = ""
}
variable "cc_sh_rds_multi_az" {
  default = ""
}
variable "skip_final_snapshot" {
  default = "false"
}


// VPC
variable "cc_sh_vpc_id" {
  
}

variable "cc_sh_allowed_vpc_cidr_blocks" {
  type    = "list"
}