// TODO: What can be done to automate the hardeing of the Web node post deployment?

provider "aws" {
  region = "${var.cc_sh_region}"
}

resource "aws_instance" "cc-web-console" {
  ami = "${var.cc_web_ami}"
  instance_type = "${var.cc_web_type}"
  iam_instance_profile = "cc-sh-s3-profile"
  associate_public_ip_address  = false

  tags = {
    Name = "${var.environment}-CC-web-console"
    Env  = "${var.environment}"
  }

}
//need to specify VPC and AZ
// do we need to create a dedicated subnet?
// definitely need to create an SG and also support multiple front ends

// CC WebConsole Security Group
// TODO: Parameterise the CIDR blocks
resource "aws_security_group" "cc-web-sg" {
  name        = "cc-web-sg"
  description = "Allow traffic to CloudCheckr web console"
  vpc_id      = "${var.cc_sh_vpc_id}"

  ingress {
    # TLS (change to whatever ports you need)
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["192.168.10.22/32"] # add a CIDR block here
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    //prefix_list_ids = ["pl-12c4e678"]
  }
}