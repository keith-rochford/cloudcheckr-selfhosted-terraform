output "CloudCheckr-Web-IP" {
    description = "IP address of the CloudChecker Web Portal"
   value = aws_instance.cc-web-console.public_ip
}

output "CloudCheckr-S3" {
    description = "S3 bucket used for CloudCheckr report data sources"
   value = aws_s3_bucket.cc-s3-data.id
}

output "CloudCheckr_Database_Endpoint" {   
  value = aws_db_instance.cc_sh_rds_db.endpoint
}

