// Create the S3 bucket for CloudCheckr (encryption keys and report data sources)
// TODO: Create a bucket policy for this (e.g. ensure that Object cant be public etc.)
// TODO: Retrict the assume_role_policy to allow only the CloudChecker EC2 instances to assume the role.
// TODO: Restrict the actions allowed under the S3 role (e.g. make sure that permissions management is removed)
// TODO: Add the bucket region and name (as required for the CloudCheckr config) as an output.
resource "aws_s3_bucket" "cc-s3-data" {
  bucket = "cloudcheckr-selfhosted-storage"
  acl    = "private"

  tags = {
    Env  = "${var.environment}"
  }
}

// Create a role and policy that allows access to the bucket

// Create the role
resource "aws_iam_role" "cc-s3-data-role" {
  name = "cc-s3-data-role"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ] 
}
EOF

  tags = {
    Env  = "${var.environment}"
  }
}

// Create the policy
resource "aws_iam_policy" "cc-s3-data-policy" {
  name = "cc-s3-data-policy"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Action": [ "s3:*" ],
            "Resource": [
                "arn:aws:s3:::cloudcheckr-selfhosted-storage",
                "arn:aws:s3:::cloudcheckr-selfhosted-storage/*"
            ] 
        }
    ] 
}
EOF

}

// Create the attachment

resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = "${aws_iam_role.cc-s3-data-role.name}"
  policy_arn = "${aws_iam_policy.cc-s3-data-policy.arn}"
}

// Create an instance profile for role the that can be used by the CloudCheckr instances

resource "aws_iam_instance_profile" "cc-sh-s3-profile" {
  name = "cc-sh-s3-profile"
  role = "cc-s3-data-role"
}
