// Create the RDS MS SQL instance for CloudCheckr self hosted

//TODO: Add the DB Server name (as required for the CloudCheckr config) as an output.

// Create the RDS DB Subnets
// TODO: Parameterise the CIDR blocks
// TODO: Parameterise the AZzzz (or do something clever to make sure each Subnt is created in a diferemce AZ)
resource "aws_subnet" "cc_sh_db_subnt1" {
  vpc_id     = "${var.cc_sh_vpc_id}"
  cidr_block = "172.31.48.0/21"
  availability_zone = "eu-west-1a"
}
resource "aws_subnet" "cc_sh_db_subnt2" {
  vpc_id     = "${var.cc_sh_vpc_id}"
  cidr_block = "172.31.56.0/21"
  availability_zone = "eu-west-1b"
}

// RDS DB Subnet Group
resource "aws_db_subnet_group" "cc_sh_rds_subnet_grp" {
   //name = "cc-rds-${var.environment}-dbsubnet"
   name = "cc_sh_rds_subnet_grp"
   description = "Subnet Group for Self-hosted CloudCheckr ${var.environment}} DB"
   subnet_ids = ["${aws_subnet.cc_sh_db_subnt1.id}", "${aws_subnet.cc_sh_db_subnt2.id}"]
}

//TODO: Change this to allow ingress from the Web SG rather than subnet if possible. 
resource "aws_security_group" "cc_sh_rds_security_group" {
  name        = "${var.environment}-all-rds-mssql-internal"
  description = "${var.environment} allow all vpc traffic to rds mssql."
  vpc_id      = "${var.cc_sh_vpc_id}"

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = "${var.cc_sh_allowed_vpc_cidr_blocks}"
  }

  //tags {
  //  Name = "${var.environment}-all-rds-mssql-internal"
  //  Env  = "${var.environment}"
  //}
}

resource "aws_db_instance" "cc_sh_rds_db" {
  depends_on                = ["aws_db_subnet_group.cc_sh_rds_subnet_grp"]
  identifier                = "cloudcheckr-selfhosted-database"
  allocated_storage         = "${var.cc_sh_db_allocated_storage}"
  license_model             = "license-included"
  storage_type              = "gp2"
  engine                    = "sqlserver-se"
  engine_version            = "12.00.4422.0.v1"
  instance_class            = "${var.cc_sh_rds_instance_class}"
  multi_az                  = "${var.cc_sh_rds_multi_az}"
  username                  = "${var.cc_sh_mssql_admin_username}"
  password                  = "${var.cc_sh_mssql_admin_password}"
  vpc_security_group_ids    = ["${aws_security_group.cc_sh_rds_security_group.id}"]
  db_subnet_group_name      = "${aws_db_subnet_group.cc_sh_rds_subnet_grp.id}"
  backup_retention_period   = 3
  skip_final_snapshot       = "${var.skip_final_snapshot}"
  final_snapshot_identifier = "${var.environment}-mssql-final-snapshot"
}

// Identifier of the mssql DB instance.
output "mssql_id" {
  value = "${aws_db_instance.cc_sh_rds_db.id}"
}

// Address of the mssql DB instance.
output "mssql_address" {
  value = "${aws_db_instance.cc_sh_rds_db.address}"
}